const mysqlx = require("@mysql/xdevapi");
const compileTipsterProfileTotals = require("../constants/compile_tipster_profile_totals");
const compileTipsterProfiles = require("../constants/compile_tipster_profiles");
const { toTimestamp } = require("../../../node_normalization/numbers");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

module.exports = async (tipsterId) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const tipsterProfileTotalTable = await mysqlDb.getTable(
    "tipster_current_profiles"
  );
  console.log(tipsterProfileTotalTable);
  let tipsterProfiles = await tipsterProfileTotalTable
    .select()
    .where("tipsterID = :tipsterID")
    .bind("tipsterID", tipsterId)
    .execute();

  tipsterProfiles = tipsterProfiles.fetchAll();
  await session.close();
  return tipsterProfiles.map((tipsterProfile) => {
    return {
      tipsterProfileID: tipsterProfile[0],
      tipsterID: tipsterProfile[1],
      periodWeeks: tipsterProfile[2],
      category: tipsterProfile[3],
      ROI: tipsterProfile[4],
      market: tipsterProfile[5],
    };
  });
};
