const mysqlx = require("@mysql/xdevapi");
const compileTipsterProfileTotals = require("./compile_tipster_profile_totals");
const compileTipsterProfiles = require("./compile_tipster_profiles");
const { toTimestamp } = require("../../../node_normalization/numbers");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

module.exports = async (tipsterId, tipster, tipsterDataLoader) => {
  let t;
  if (!tipster) {
    t = await tipsterDataLoader.load(tipsterId);
  } else {
    t = tipster;
  }

  return {
    tipsterID: t[0],
    url: t[1],
    profilePic: t[2],
    lastUpdated: t[3],
    platform: t[4],
    handle: t[5],
    stakingFactor: t[6],
    fakeCount: t[7],
    numOfTips: t[8],
    tipsterSkatingFactorID: t[9],
    profileTotal: await compileTipsterProfiles(tipsterId),
    profile: await compileTipsterProfileTotals(tipsterId),
  };
};
