const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");
const getTipsterById = async (id) => {
  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  const tipsterTable = await mysqlDb.getTable("tipsters");
  let tipster = await tipsterTable
    .select()
    .where("tipsterID = :tipsterID")
    .bind("tipsterID", id)
    .execute();

  tipster = tipster.fetchOne();
  await session.close();
  return tipster;
};

module.exports.getTipsterByIds = async (ids) => {
  return ids.map((id) => getTipsterById(id));
};
