/*
    User resolvers
*/
// resolver functions

const addTip = require("./resolvers/add_tip");
const updateTipster = require("./resolvers/update_tipster");
const updateTip = require("./resolvers/update_tip");
const getTips = require("./resolvers/get_tips");
const normalizeBets = require("./resolvers/normalize_bets");
const { pubsub } = require("../../constants/pubsub");

module.exports = {
  Subscription: {
    changedTip: {
      subscribe: () => pubsub.asyncIterator(["CHANGED_TIP"]),
    },
  },
  Mutation: {
    addTip,
    updateTipster,
    updateTip,
  },
  Query: {
    getTips,
    normalizeBets,
  },
};
