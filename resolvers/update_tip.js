const { pubsub } = require("../../../constants/pubsub");
const queryBuilder = require("../../../data/mysql/query_builder");
const {
  addDataToSheet,
} = require("../../../google_sheet_actions/add_to_sheet_queue");
const { MYSQL_DATABASE } = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const { toTimestamp } = require("../../../node_normalization/numbers");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
const calcProfit = (odds, units, result, action) => {
  //////////console.log(odds, units, result);

  if (action === "fade") {
    switch (result.toLowerCase()) {
      case "win":
        return [-units, (odds - 1) * units, "Loss"];
      case "lost":
      case "loss":
        return [(odds - 1) * units, -units, "Win"];
      case "half win":
        return [-units / 2, ((odds - 1) * units) / 2, "Half Loss"];
      case "half lost":
      case "half loss":
        return [((odds - 1) * units) / 2, -units / 2, "Half Win"];
      case "push":
      case "void":
        return [0, 0, result];

      default:
        throw new Error(`Result ${result} not found`);
    }
  } else {
    switch (result.toLowerCase()) {
      case "win":
        return [(odds - 1) * units, (odds - 1) * units, "Win"];
      case "loss":
      case "lost":
        return [-units, -units, "Loss"];
      case "half win":
        return [((odds - 1) * units) / 2, ((odds - 1) * units) / 2, "Half Win"];
      case "half lost":
      case "half loss":
        return [-units / 2, -units / 2, "Half Loss"];
      case "push":
      case "void":
        return [0, 0, result];

      default:
        throw new Error(`Result ${result} not found`);
    }
  }
};
module.exports = async (_, { inputTip }, { currentUser }) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }

  console.log("1");

  let session;
  try {
    session = await client.getSession();
  } catch (err) {
    client = await reconnect();
    session = await client.getSession();
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  try {
    console.log("2");
    const tipColl = await mysqlDb.getCollection("tips");

    const tipsterTable = await mysqlDb.getTable("tipsters");

    const tipsterCurrentProfilesTable = await mysqlDb.getTable(
      "tipster_current_profiles"
    );

    const tipsterProfilesTable = await mysqlDb.getTable("tipster_profiles");

    let query = inputTip.url
      ? `url like '${inputTip.url}'`
      : `_id like '${inputTip._id}'`;
    let oldTip = await tipColl.find(query).execute();
    console.log(oldTip);
    oldTip = oldTip.fetchOne();
    console.log(oldTip);
    if (!oldTip) {
      return null;
    }

    if (
      inputTip.result &&
      inputTip.result === oldTip.result &&
      inputTip.url &&
      (!inputTip.valid || !inputTip.avaialable)
    ) {
      return oldTip;
    }
    if (
      inputTip.result &&
      inputTip.result !== "" &&
      inputTip.result !== oldTip.result
    ) {
      const unitsDifference = calcProfit(
        oldTip.odds,
        oldTip.units,
        inputTip.result,
        oldTip.action
      );
      ////////console.log(unitsDifference);
      inputTip.unitsDifference = unitsDifference[0];
      inputTip.originalDifference = unitsDifference[1];
      inputTip.actionResult = unitsDifference[2];
      console.log(inputTip);
    }

    console.log(inputTip);
    console.log("here");
    await tipColl
      .modify(query)
      .patch({
        ...inputTip,
        settledDate: toTimestamp(new Date()),
        tipsterID: oldTip.tipster.tipsterID,
      })
      .execute();
    let newTip = await tipColl.find(query).execute();
    newTip = newTip.fetchOne();
    pubsub.publish("TIP_CHANGED", {
      changedTip: { ...newTip },
    });
    /*
    if (newTip.action !== "leave") {
      addDataToSheet.add({
        service: "update_tip",
        data: {
          tip: { ...newTip },
        },
        sheet: "11YKJBlTpYHe2X9qAhCASKiseTllw3N-ZVA6lrUysdAE",
      });
    }*/

    if (newTip.tipster.platform === "blogabet") {
      const user_table = await mysqlDb.getTable("users");
      let user = await user_table.select().where("userID = 2").execute();
      user = user.fetchOne();
      addDataToSheet.add({
        service: "update_master_tip",
        data: {
          tip: { ...newTip },
        },
        sheet: "1TQgqKTDWq6FmquRvAJ8U6lh14a61sZ_67YQTKw1xviQ",
      });
    }

    if (inputTip.originalDifference && newTip.originalOdds) {
      let totalProfile = await tipsterProfilesTable
        .select()
        .where(
          `tipsterID = ${newTip.tipster.tipsterID} AND category = '${newTip.platformCategory}'`
        )
        .execute();
      totalProfile = totalProfile.fetchOne();
      console.log(totalProfile);
      let ROI;
      if (!totalProfile) {
        ROI = (newTip.originalDifference / newTip.units) * 100;
        await queryBuilder(
          session,
          "insert",
          {
            tipsterID: newTip.tipster.tipsterID,
            category: newTip.platformCategory,
            market: "",
            ROI: ROI,
            unitsRisked: newTip.units,
            profileMade: newTip.originalDifference,
          },
          "tipster_profiles"
        );
      } else {
        ROI =
          ((totalProfile[6] + newTip.originalDifference) /
            (totalProfile[5] + newTip.units)) *
          100;
        await queryBuilder(
          tipsterProfilesTable,
          "update",
          {
            ROI: ROI,
            unitsRisked: totalProfile[5] + newTip.units,
            profileMade: totalProfile[6] + newTip.originalDifference,
          },
          null,
          "tipsterProfileTotalID",
          totalProfile[0]
        );
      }

      let settledTips = await tipColl
        .find(
          `tipsterID = ${newTip.tipster.tipsterID} AND settledDate > ${
            toTimestamp(new Date()) - 86400 * 14
          }`
        )
        .execute();

      settledTips = settledTips.fetchAll();
      let units = 0;
      let profit = 0;

      for (let i = 0; i < settledTips.length; i++) {
        profit = profit + settledTips[i].originalDifference;
        units = units + settledTips[i].units;
      }

      ROI = (profit / units) * 100;

      let profile = await tipsterCurrentProfilesTable
        .select()
        .where(
          `tipsterID = ${newTip.tipster.tipsterID} AND category = '${newTip.platformCategory}'`
        )
        .execute();
      profile = profile.fetchOne();
      console.log(profile);
      if (!profile) {
        ROI = (newTip.originalDifference / newTip.units) * 100;
        await queryBuilder(
          session,
          "insert",
          {
            tipsterID: newTip.tipster.tipsterID,
            category: newTip.platformCategory,
            market: "",
            ROI: ROI,
            periodWeeks: 2,
            unitsRisked: newTip.units,
            profileMade: newTip.originalDifference,
          },
          "tipster_current_profiles"
        );
      } else {
        await queryBuilder(
          tipsterCurrentProfilesTable,
          "update",
          {
            ROI: ROI,
            unitsRisked: units,
            profileMade: profit,
          },
          null,
          "tipsterProfileID",
          profile[0]
        );
      }

      totalProfile = await tipsterProfilesTable
        .select()
        .where(
          `tipsterID = ${newTip.tipster.tipsterID} AND category = '${newTip.platformSport}'`
        )
        .execute();
      totalProfile = totalProfile.fetchOne();
      console.log(totalProfile);
      if (!totalProfile) {
        ROI = (newTip.unitsDifference / newTip.units) * 100;
        await queryBuilder(
          session,
          "insert",
          {
            tipsterID: newTip.tipster.tipsterID,
            category: newTip.platformSport,
            market: "",
            ROI: ROI,
            unitsRisked: newTip.units,
            profileMade: newTip.unitsDifference,
          },
          "tipster_profiles"
        );
      } else {
        ROI =
          ((totalProfile[6] + newTip.unitsDifference) /
            (totalProfile[5] + newTip.units)) *
          100;
        await queryBuilder(
          tipsterProfilesTable,
          "update",
          {
            ROI: ROI,
            unitsRisked: totalProfile[5] + newTip.units,
            profileMade: totalProfile[6] + newTip.unitsDifference,
          },
          null,
          "tipsterProfileTotalID",
          totalProfile[0]
        );
      }

      units = 0;
      profit = 0;

      for (let i = 0; i < settledTips.length; i++) {
        profit = profit + settledTips[i].unitsDifference;
        units = units + settledTips[i].units;
      }

      ROI = (profit / units) * 100;

      profile = await tipsterCurrentProfilesTable
        .select()
        .where(
          `tipsterID = ${newTip.tipster.tipsterID} AND category = '${newTip.platformSport}'`
        )
        .execute();
      profile = profile.fetchOne();
      console.log(profile);
      if (!profile) {
        ROI = (newTip.unitsDifference / newTip.units) * 100;
        await queryBuilder(
          session,
          "insert",
          {
            tipsterID: newTip.tipster.tipsterID,
            category: newTip.platformSport,
            market: "",
            ROI: ROI,
            periodWeeks: 2,
            unitsRisked: newTip.units,
            profileMade: newTip.unitsDifference,
          },
          "tipster_current_profiles"
        );
      } else {
        await queryBuilder(
          tipsterCurrentProfilesTable,
          "update",
          {
            ROI: ROI,
            unitsRisked: units,
            profileMade: profit,
          },
          null,
          "tipsterProfileID",
          profile[0]
        );
      }
    }
    await session.close();
    return newTip;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Updating tip",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
