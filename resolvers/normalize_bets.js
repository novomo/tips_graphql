const {
  normalizeBlogabet,
  normalizeOLBG,
  normalizeTipsterland,
} = require("../constants/normalize_bets");
const IP = require('ip');
const upload_error = require("../../../node_error_functions/upload_error")
module.exports = async (_, { query }, { currentUser }) => {
  if (!currentUser) {
    throw new Error("Not Authorized");
  }
  try {
    console.log(query);
    let { events, bets, platform } = JSON.parse(query);
    let finalBets;
    if (platform === "blogabet") {
      finalBets = normalizeBlogabet(bets, events);
    } else if (platform === "tipsterland") {
      finalBets = normalizeTipsterland(bets, events);
    } else if (platform === "olbg") {
      finalBets = normalizeOLBG(bets, events);
    }
    return finalBets;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Normailizing Bet",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true
    })

  }
};
