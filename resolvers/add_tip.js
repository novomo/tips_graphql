const compileTipster = require("../constants/compile_tipster");
const { findByValueOfObject } = require("../../../node_normalization/filters");
const { formatDate } = require("../../../node_normalization/dates");
const queryBuilder = require("../../../data/mysql/query_builder");
const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
const { pubsub } = require("../../../constants/pubsub");
const {
  addDataToSheet,
} = require("../../../google_sheet_actions/add_to_sheet_queue");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
const webhook = require("webhook-discord");
module.exports = async (
  _,
  { inputTip },
  { currentUser, sportEventDataLoader, tipsterDataLoader }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }

  let session;
  console.log(client);
  try {
    session = await client.getSession();
  } catch (err) {
    console.log(err);
    client = await reconnect();
    session = await client.getSession();
    console.log(client);
  }
  const mysqlDb = await session.getSchema(MYSQL_DATABASE);
  try {
    const tipColl = await mysqlDb.getCollection("tips");
    let tip = await tipColl.find(`url = '${inputTip.url}'`).execute();
    tip = tip.fetchOne();
    console.log("tip");
    console.log(tip);
    if (tip) {
      return tip;
    }

    const tipsterTable = await mysqlDb.getTable("tipsters");
    let tipster = await tipsterTable
      .select()
      .where("url = :url")
      .bind("url", inputTip.tipster)
      .execute();
    tipster = tipster.fetchOne();
    if (!tipster) {
      throw new Error("tipster not in database!");
    }

    tipster = await compileTipster(tipster[0], null, tipsterDataLoader);
    console.log(tipster);
    let sportRatioTotal, categoryRatioTotal, sportRatios, categoryRatios;
    if (tipster.profileTotal) {
      sportRatioTotal = findByValueOfObject(
        "category",
        inputTip.platformSport,
        tipster.profileTotal
      );

      sportRatios = findByValueOfObject(
        "category",
        inputTip.platformSport,
        tipster.profile
      );

      sportRatios = findByValueOfObject("periodWeeks", 2, sportRatios);

      categoryRatioTotal = findByValueOfObject(
        "category",
        inputTip.platformCategory,
        tipster.profileTotal
      );

      let categoryRatios = findByValueOfObject(
        "category",
        inputTip.platformCategory,
        tipster.profile
      );

      categoryRatios = findByValueOfObject("periodWeeks", 2, categoryRatios);
    }
    let action = "leave";
    if (tipster.profileTotal && tipster.profileTotal.length > 0) {
      if (tipster.numOfTips <= 20) {
        action = "leave";
      } else if (
        tipster.numOfTips < 200 &&
        categoryRatioTotal.length === 0 &&
        tipster.numOfTips > 20
      ) {
        console.log(categoryRatioTotal);
        if (!sportRatioTotal[0]) {
          action = "leave";
        } else if (
          sportRatioTotal[0].ROI < -8 &&
          categoryRatioTotal.length === 0
        ) {
          action = "fade";
        } else if (
          sportRatioTotal[0].ROI > 8 &&
          categoryRatioTotal.length === 0
        ) {
          action = "tail";
        }
      } else if (tipster.numOfTips < 200 && categoryRatioTotal.length > 0) {
        if (!sportRatioTotal[0]) {
          action = "leave";
        } else if (
          sportRatioTotal[0].ROI > 6 &&
          categoryRatioTotal[0].ROI > 8
        ) {
          action = "tail";
        } else if (
          sportRatioTotal[0].ROI < -6 &&
          categoryRatioTotal[0].ROI < -8
        ) {
          action = "fade";
        }
      } else {
        if (
          sportRatioTotal[0].ROI > 4 &&
          categoryRatioTotal[0].ROI > 6 &&
          sportRatios[0].ROI > 10 &&
          categoryRatios[0].ROI > 10
        ) {
          action = "tail";
        } else if (
          sportRatioTotal[0].ROI < -4 &&
          categoryRatioTotal[0].ROI < -6 &&
          sportRatios[0].ROI < -10 &&
          categoryRatios[0].ROI < -10
        ) {
          action = "fade";
        }
      }
    }

    const events = await Promise.all(
      inputTip.events.map(async (event) => {
        if (event.eventID) {
          return await sportEventDataLoader.load(event.eventID);
        } else {
          return event;
        }
      })
    );
    let finalBets = inputTip.bets;

    let tipsterProfile = [];
    let tipsterProfileTotal = [];

    if (sportRatios) {
      tipsterProfile = [...tipsterProfile, ...sportRatios];
    }
    if (categoryRatios) {
      tipsterProfile = [...tipsterProfile, ...categoryRatios];
    }
    if (categoryRatioTotal) {
      tipsterProfileTotal = [...tipsterProfileTotal, ...categoryRatioTotal];
    }
    if (sportRatioTotal) {
      tipsterProfileTotal = [...tipsterProfileTotal, ...sportRatioTotal];
    }
    let message;

    let impliedProb = (1 / inputTip.odds) * 100;
    let sideImplied = (100 - impliedProb) * 1.09;
    let fadeOdds = 100 / sideImplied;

    let bet_string = ``;
    const event_text_split = inputTip.originalText.split("@");
    message = `${inputTip.originalText}
      
action: ${action} @ ${
      action === "fade" ? fadeOdds.toFixed(2) : inputTip.odds.toFixed(2)
    } for ${
      !tipster.stakeFactor
        ? 1
        : (tipster.stakingFactor * inputTip.units).toFixed(2)
    } unit/s`;

    let tipID = await tipColl
      .add({
        ...inputTip,
        action: action,
        odds:
          action === "fade" ? fadeOdds.toFixed(2) : inputTip.odds.toFixed(2),
        originalOdds: inputTip.odds,
        units: !tipster.stakingFactor
          ? 1
          : tipster.stakingFactor * inputTip.units,
        bets: finalBets.map((bet, index) => {
          let eventDetails = {};
          if (inputTip.bets[index].eventType === "eSports") {
            eventDetails.eSportsEvent = events[index];
          } else if (inputTip.bets[index].eventType === "sports") {
            eventDetails.sportsEvent = events[index];
          }
          return {
            ...bet,
            event: eventDetails,
          };
        }),
        tipsterProfile: tipsterProfile,
        tipsterProfileTotal: tipsterProfileTotal,
        tipster: tipster,
        results: "",
        messageId: "",
        notificationId: "",
        discordMsg: message,
        result: inputTip.result ? inputTip.result : "",
      })
      .execute();

    staking_collection = await mysqlDb.getCollection("tipster_staking_factors");
    let final_stakes;
    let update_tipser = {
      numOfTips: tipster.numOfTips ? tipster.numOfTips + 1 : 1,
    };
    if (tipster.tipsterSkatingFactorID) {
      let staking_factors = await staking_collection
        .find(`_id = '${tipster.tipsterSkatingFactorID}'`)
        .execute();
      staking_factors = staking_factors.fetchOne();
      console.log(staking_factors);
      const stakeIndex = staking_factors.stakes.findIndex(function (stake) {
        //console.log(stake);
        return stake.stake == inputTip.units;
      });
      console.log(stakeIndex);
      let stakes = [...staking_factors.stakes];
      if (stakeIndex === -1) {
        stakes.push({
          stake: inputTip.units,
          times: 1,
        });
        final_stakes = stakes;
      } else {
        let stke = {
          stake: stakes[stakeIndex].stake,
          times: stakes[stakeIndex].times + 1,
        };
        stakes[stakeIndex] = stke;
        console.log(stakes);
        ////console.log(tipster._doc.stakes);
        let stakeFactor;
        for (let i = 0; i < stakes.length; i++) {
          ////console.log(stakeFactor);
          ////console.log(tipster._doc.stakes[i]);

          if (
            !stakeFactor ||
            (stakes[i].times >= stakeFactor.times * 0.4 &&
              stakes[i].stake > stakeFactor.stake)
          ) {
            ////console.log("adding");
            stakeFactor = stakes[i];
          }
        }
        ////console.log(stakeFactor);
        update_tipser.stakingFactor = 1 / stakeFactor.stake;
        final_stakes = stakes;
        ////console.log(tipsterObj.stakes);
      }
      await staking_collection
        .modify(`_id = '${tipster.tipsterSkatingFactorID}'`)
        .patch({ stakes: final_stakes })
        .execute();
    } else {
      let stakes = [];
      stakes.push({
        stake: inputTip.units,
        times: 1,
      });
      final_stakes = stakes;
      update_tipser.stakingFactor = parseFloat(1 / inputTip.units);

      let newTipsterSkatingFactorID = await staking_collection
        .add({
          tipsterID: tipster.tipsterID,
          stakes: final_stakes,
        })
        .execute();

      console.log(newTipsterSkatingFactorID);

      update_tipser.tipsterSkatingFactorID =
        newTipsterSkatingFactorID.getGeneratedIds()[0];
    }

    await queryBuilder(
      tipsterTable,
      "update",
      update_tipser,
      null,
      "tipsterID",
      tipster.tipsterID
    );

    console.log(tipID);
    tipID = tipID.getGeneratedIds()[0];

    let finalTip = await tipColl
      .find("_id like :tipID")
      .bind("tipID", tipID)
      .execute();

    finalTip = finalTip.fetchOne();

    pubsub.publish("TIP_CHANGED", {
      changedTip: { ...finalTip },
    });
    const user_table = await mysqlDb.getTable("users");
    let user = await user_table.select().where("userID = 2").execute();
    user = user.fetchOne();

    addDataToSheet.add({
      service: "add_master_tip",
      data: {
        tip: { ...finalTip, tipster: tipster.handle },
      },
      sheet: user[6],
    });
    /*if (action !== "leave") {
      addDataToSheet.add({
        service: "add_tip",
        data: {
          tip: { ...finalTip },
        },
        sheet: "11YKJBlTpYHe2X9qAhCASKiseTllw3N-ZVA6lrUysdAE",
      });
    }*/
    await session.close();
    return finalTip;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Adding Tip",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
