const queryBuilder = require("../../../data/mysql/query_builder");
const { MYSQL_DATABASE } = require("../../../env.js");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
module.exports = async (_, { inputTipster }, { currentUser }) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {
    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const tipsterTable = await mysqlDb.getTable("tipsters");

    let q;
    if (inputTipster.tipsterID) {
      q = `tipsterID = ${nputTipster.tipsterID}`;
    } else {
      q = `url = "${inputTipster.url}"`;
    }
    let tipster = await tipsterTable.select().where(q).execute();

    tipster = tipster.fetchOne();
    if (tipster) {
      await queryBuilder(
        tipsterTable,
        "update",
        inputTipster,
        null,
        inputTipster.tipsterID ? "tipsterID" : "url",
        inputTipster.tipsterID ? inputTipster.tipsterID : inputTipster.url
      );
    } else {
      await queryBuilder(session, "insert", inputTipster, "tipsters");
    }
    await session.close();
    return true;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Updating tipster",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
