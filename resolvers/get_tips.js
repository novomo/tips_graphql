const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
  DISCORD_MESSAGE,
} = require("../../../env.js");
const { pubsub } = require("../../../constants/pubsub");
let { client, reconnect } = require("../../../data/mysql/sql_connection");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
module.exports = async (_, { query }, { currentUser }) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {
    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE);
    const tipColl = await mysqlDb.getCollection("tips");

    let tips = await tipColl
      .find(JSON.parse(query.replace(/'/g, '"')))
      .execute();
    tips = tips.fetchAll();
    await session.close();
    return tips;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Getting Tip",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
