const findByValueOfObject = (key, value, obj) => {
  return obj.filter(function (item) {
    return item[key] === value;
  });
};

const match = (dict, phrase, market, main = false) => {
  console.log("bet");
  console.log(market);
  console.log("Main");
  console.log(main);
  console.log(dict);
  for (const [key, value] of Object.entries(dict)) {
    console.log(value);
    if (
      new RegExp(/half(\s|)time(\s|)\/(\s|)full(\s|)time/i).test(market) &&
      (key === "Half" || key === "Halftime")
    ) {
      continue;
    }

    if (
      new RegExp(/half\swith\smost\sgoals/i).test(market) &&
      (key === "Second" || key === "First")
    ) {
      continue;
    }
    if (new RegExp(/\sgoals/i).test(market) && key === "Goals") {
      continue;
    }
    if (new RegExp(/set\sbetting/i).test(market) && key === "Set") {
      continue;
    }
    const re = new RegExp(value);
    console.log(re);
    console.log(phrase);
    console.log(re.test(phrase.toLowerCase()));
    if (re.test(phrase.toLowerCase())) {
      return [key, true];
    }
  }
  return [market, false];
};

const normaliseBet = (bet, event, market) => {
  const { homeTeam, awayTeam } = event;
  let re, value, valueArray;
  let miniBet = bet.toLowerCase();
  console.log(miniBet);
  const miniHomeTeam = homeTeam.toLowerCase();
  console.log(miniHomeTeam);
  const miniAwayTeam = awayTeam.toLowerCase();
  console.log(miniAwayTeam);
  const miniMarket = market.toLowerCase();
  console.log(miniBet);
  let directRe;
  switch (market) {
    case "Correct Score":
      return [
        bet
          .replace(/#/g, "")
          .replace("Draw ", "")
          .replace(`${homeTeam} `, " ")
          .replace(`${awayTeam} `, " "),
        true,
      ];
    case "Under/Over":
      re = new RegExp(/^(\+|\-|)[0-9]{1,3}.*/i);
      valueArray = miniBet.split(" ");
      console.log(valueArray);
      for (const v in valueArray) {
        console.log(valueArray[v]);
        if (re.test(valueArray[v].toLowerCase())) {
          value = valueArray[v].replace(/\+/g, "").replace(/\-/g, "");
          break;
        }
      }
      directRe = new RegExp(/(^|\s)(under|u)\s/i);
      if (directRe.test(miniBet)) {
        return [`Under ${value}`, true];
      } else {
        return [`Over ${value}`, true];
      }
    case "Asian Handicap":
    case "Handicap":
    case "Spread":
      directRe = new RegExp(
        /(\+|\-|)[0-9]{1}.[0-9]{1,2}\,(\s|)(\+|\-|)[0-9]{1}.[0-9]{1,2}/i
      );
      re = new RegExp(/^(\+|\-|)[0-9]{1,3}.*/i);
      splitRe = new RegExp(/(\+|\-|)[0-9]{1}.[0-9]{1,2}\,|(\s|(\+|\-)|)/i);
      console.log(directRe.test(miniBet));
      if (directRe.test(miniBet)) {
        console.log(miniBet);
        //miniBet.replace(", ", ",");
        //console.log(miniBet);
        miniBet.replace(",", ", ");
        let splitRegex = /(\s|\,)/i;
        miniBet.replace(/\,/i, ", ");
        console.log(miniBet.split(splitRegex));
        valueArray = miniBet.split(splitRegex);
        for (let v = 0; v < valueArray.length; v++) {
          if (re.test(valueArray[v].toLowerCase())) {
            value = `${valueArray[v]} ${
              valueArray[v + 1].length > 1
                ? valueArray[v + 1]
                : valueArray[v + 2]
            }`;
            console.log("value");
            console.log(value);
            break;
          }
        }
      } else {
        valueArray = miniBet.split(" ");
        console.log(valueArray);
        for (const v in valueArray) {
          if (
            re.test(
              valueArray[v].toLowerCase().replace("(", "").replace(")", "")
            )
          ) {
            value = valueArray[v]
              .toLowerCase()
              .replace("(", "")
              .replace(")", "");
            console.log("value");
            break;
          }
        }
      }

      if (miniBet.includes("home") || miniBet.includes(miniHomeTeam)) {
        return [`${homeTeam} ${value}`, true];
      } else {
        return [`${awayTeam} ${value}`, true];
      }
    case "Moneyline":
    case "Draw No Bet":
    case "Win To Nil":
      if (miniBet.includes("home") || miniBet.includes(miniHomeTeam)) {
        return [`${homeTeam}`, true];
      } else {
        return [`${awayTeam}`, true];
      }
    case "Match Odds":
      if (miniBet.includes("home") || miniBet.includes(miniHomeTeam)) {
        return [`${homeTeam}`, true];
      } else if (miniBet.includes("away") || miniBet.includes(miniAwayTeam)) {
        return [`${awayTeam}`, true];
      } else {
        return [`Draw`, true];
      }
    case "Race to":
      valueArray = miniBet.split(/(\D*\d)(.*)$/);
      value = valueArray[1];
      if (miniBet.includes("home") || miniBet.includes(miniHomeTeam)) {
        return [`${homeTeam} ${value}`, true];
      } else {
        return [`${awayTeam} ${value}`, true];
      }
    case "Team Totals":
      re = new RegExp(/^(\+|\-|)[0-9]{1,3}.*/i);
      valueArray = miniBet.split(" ");
      for (const v in valueArray) {
        if (re.test(valueArray[v].toLowerCase())) {
          value = valueArray[v].replaceAll("+", "").replaceAll("-", "");
          break;
        }
      }
      let team;
      if (miniBet.includes("home") || miniBet.includes(miniHomeTeam)) {
        team = homeTeam;
      } else {
        team = awayTeam;
      }
      if (miniBet.includes(" over ") || miniBet.includes(" o ")) {
        return [`${team} Over ${value}`, true];
      } else {
        return [`${team} Under ${value}`, true];
      }
    case "Fight Outcome":
      if (miniBet.includes("home") || miniBet.includes(miniHomeTeam)) {
        return [`${homeTeam}`, true];
      } else {
        return [`${awayTeam}`, true];
      }

    case "BTTS":
      if (miniBet.includes("yes")) {
        return ["Yes", true];
      } else {
        return ["No", true];
      }
    case "Match Odds & BTTS":
      if (miniBet.includes("home") || miniBet.includes(miniHomeTeam)) {
        if (miniBet.includes("yes")) {
          return [`${homeTeam} & Yes`, true];
        } else {
          return [`${homeTeam} & No`, true];
        }
      } else if (miniBet.includes("away") || miniBet.includes(miniAwayTeam)) {
        if (miniBet.includes("yes")) {
          return [`${awayTeam} & Yes`, true];
        } else {
          return [`${awayTeam} & No`, true];
        }
      } else {
        if (miniBet.includes("yes")) {
          return ["Draw & Yes", true];
        } else {
          return ["Draw & No", true];
        }
      }
    case "Score in Both Halves":
      if (miniBet.includes("home") || miniBet.includes(miniHomeTeam)) {
        return [`${homeTeam}`, true];
      } else if (miniBet.includes("away") || miniBet.includes(miniAwayTeam)) {
        return [`${awayTeam}`, true];
      }
    case "Half with Most Goals":
      if (new RegExp(/(1st|first)/i).test(bet)) {
        return ["First Half", true];
      } else if (new RegExp(/(2nd|second)/i).test(bet)) {
        return ["Second Half", true];
      } else {
        return ["Tie", true];
      }
    case "Set Betting":
      return [bet.replace(" to win", ""), true];
    case "Halftime/Fulltime":
    case "Anytime Goalscorer":
    case "First Goalscorer":
    case "Man Of The Match":
    case "Double Chance":
    case "9 Dart finish in match":
    case "Clean Sheet":
      return [bet, true];

    default:
      break;
  }
  //const Hook = new webhook.Webhook(process.env.DISCORD_ERROR);
  //Hook.info("Normalise Bet", `Bet: ${bet}. \n Could not be normalised.`);
  return [bet, false];
};

const normalizeOLBG = (bets, events) => {
  const marketAreaDict = {
    Cards: `cards`,
    Games: `games`,
    Goals: /goals/i,
    "180s": /180s/i,
    Legs: /legs/i,
    Corners: /corners/i,
    Points: /points/i,
    Sets: `sets`,
    Hits: `hits`,
  };
  const typeDict = {
    Quarter: `quarter`,
    Halftime: `\\sht|half\\stime`,
    Half: /(half|fh|1hf)/i,
    Period: `period`,
    Set: `set`,
    Match: "(ft |match)",
    Inning: "inning",

    Map: "map",
    "Extra Time": "extrastime",
    "Extra Inning": "extrainnning",
  };
  const numDict = {
    First: `\\sht|first|1st|set\\s1|fh|1hf`,
    Second: `2nd|second|set\\s2`,
    Third: `3rd|third|set\\s3`,
    Fourth: `4th|fourth|set\\s4`,
    Fifth: `5th|fifth|set\\s5`,
    Sixth: `6th|sixth|set\\s6`,
    Seventh: `7th|seventh|set\\s7`,
    Eighth: `8th|eighth|set\\s8`,
    Ninth: `9th|ninth|set\\s9`,
  };
  let normailisedBets = [];

  for (let i = 0; i < bets.length; i++) {
    let event = findByValueOfObject("_id", bets[i].eventID, events);
    event = event[0];
    console.log(event);
    const marketDict = {
      "Set Betting": /set\sbetting/i,
      "Man Of The Match": /man\sof\sthe\smatch/i,
      "9 Dart finish in match": /9\sdart\sfinish\sin\smatch/i,
      "Double Chance": /double\schance/i,
      "Clean Sheet": /clean\ssheet/i,
      "Score in Both Halves": /score\sin\sboth\shalves/i,
      "Halftime/Fulltime": /half(\s|)time(\s|)\/(\s|)full(\s|)time/,
      "Match Odds & BTTS": /result\s(\&|and)\s(both\steams\sto\sscore|btts)/,
      "Match Odds": `(1x2|\\sresult|fulltime\\sresult|most\\s|win\\smatch)`,
      BTTS: /(both\steam(|s)\sto\sscore|btts)/i,
      "Team Totals": `(ttg|team\\stotal)`,
      "Under/Over": /(over|under)|(o|u)\s\d/i,
      "Half with Most Goals": /half\swith\smost\sgoals/i,
      "Asian Handicap":
        /(\sah|asian\s(handicap|hcap)|(ah\s(\+|\-)\d|(\+|\-)\d(|\.\d)\sah)|\(Handicap\s\(incl\.\sovertime\)\))/i,
      Handicap: /((|european\s)handicap(\s|\))|games\swon)/i,
      Spread:
        /(spread\s|run(\s|)line|puck(\s|)line|(\-|\+)\d(\d\.(\d||\d\d)|\.(\d||\d\d)\s\(\d[a-z][a-z]\s))/i,
      "Win To Nil": /win\sto\snil/i,
      "Race to": `race`,
      "Draw No Bet": /draw\sno\sbet/i,
      "Fight Outcome": `fight\\soutcome`,
      "Correct Score":
        /(\d(|\d)(\s\:\s|\:\s|\s\:)\d(|\d)|correct\sscore|half\stime\sscore)/i,
      "Anytime Goalscorer": /anytime\sgoalscorer/i,
      "First Goalscorer": /first\sgoalscorer/,
      Moneyline: `(win\sfight|money line|\\sml|to\\swin\\smatch|${event.homeTeam.toLowerCase()}|${event.awayTeam.toLowerCase()})`,
    };
    let betDetails = { ...bets[i] };
    // market
    let marketPhrase = bets[i].market;
    console.log(marketPhrase);
    let betPhrase = bets[i].bet;
    console.log(betPhrase);
    let market = match(
      marketDict,
      `${marketPhrase} ${betPhrase}`,
      marketPhrase,
      (main = true)
    );

    // market area
    let marketArea = match(
      marketAreaDict,
      `${marketPhrase} ${betPhrase}`,
      marketPhrase
    );

    let marketPeriod = match(
      typeDict,
      `${marketPhrase} ${betPhrase}`,
      marketPhrase
    );

    let marketPeriodNum = match(
      numDict,
      `${marketPhrase} ${betPhrase}`,
      marketPhrase
    );

    let finalMarket = "";
    if (marketPeriodNum[1]) {
      finalMarket = finalMarket + marketPeriodNum[0] + " ";
    }
    if (marketPeriod[1] && marketPeriod[0] !== "Match") {
      finalMarket = finalMarket + marketPeriod[0] + " ";
    }

    if (market[1]) {
      finalMarket = finalMarket + market[0] + " ";
    }
    if (marketArea[1]) {
      finalMarket = finalMarket + marketArea[0] + " ";
    }
    finalMarket = finalMarket.trim();
    betDetails.market = finalMarket;
    console.log(market);
    let finalBet = normaliseBet(betPhrase, event, market[0]);

    betDetails.bet = finalBet[0];

    if (!normaliseBet[1]) {
    }

    normailisedBets.push(betDetails);
  }

  return normailisedBets;
};

const bets = [
  {
    market: "Japan -10.5 (Game Lines) (4-1)",
    bet: "Japan -10.5 ",
  },
];

const events = [
  {
    competition: "Basketball",
    homeTeam: "Japan",
    awayTeam: "Hamrun Spartans",
  },
];
const results = normalizeOLBG(bets, events);

console.log(results);
